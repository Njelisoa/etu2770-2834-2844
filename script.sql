create table Utilisateur (
    id int primary key  auto_increment,
    mail varchar (30) not null ,
    motDePasse VARCHAR (255) not null ,
    statut int check (statut=0 or statut=1)
);

create table VarieT ( 
    id int primary key  auto_increment,
    nom varchar(30) not null,
    rendement double default 10,
    space double,
    CHECK (rendement>=0)
);

create table parcelle (
    id int primary key auto_increment,
    taille double ,
    idVarieT int REFERENCES VarieT(id),
    Dates date not null
);

create table Cueilleur (
    id int primary key  auto_increment,
    nom varchar (40),
    Dtn date,
    genre int check (genre=0 or genre=1)
);


create table Cueillette (
    id int primary key  auto_increment,
    idParcelle int REFERENCES parcelle(id),
    quantite double DEFAULT 0 ,
    idCueilleur int,
    Dates date not null ,
    CHECK (quantite>=0)
);

create table salaire (
    salaire double DEFAULT 5000,
    Dates date not null
);

create table CategorieDepense (
    id int primary key  auto_increment,
    nom VARCHAR (30)
);

create table Depense (
    id int primary key  auto_increment,
    idCategorieDepense int,
    Depense double,
    Dates date,
    CHECK (Depense>=0),
    Foreign key (idCategorieDepense) REFERENCES CategorieDepense(id)
);

INSERT INTO Utilisateur (mail, motDePasse, statut) VALUES ('admin@gmail.com', sha1('admin'), 0);
INSERT INTO Utilisateur (mail, motDePasse, statut) VALUES ('utilisateur@gmail.com', sha1('utilisateur'), 1);

INSERT INTO parcelle (taille, idVarieT, Dates) VALUES (10.2, 1, '2024-02-12');
INSERT INTO parcelle (taille, idVarieT, Dates) VALUES (60.2, 2, '2024-02-12');

INSERT INTO Cueilleur (id,nom) VALUES (default,'Matio');
INSERT INTO Cueilleur (id,nom) VALUES (default,'Mark');
INSERT INTO Cueilleur (id,nom) VALUES (default,'Lioka');

INSERT INTO salaire (salaire, Dates) VALUES (5000, '2024-02-01');

INSERT INTO CategorieDepense (nom) VALUES ('engrais');
INSERT INTO CategorieDepense (nom) VALUES ('Transport');

INSERT INTO Depense (idCategorieDepense, Depense, Dates) VALUES (1, 50.75, '2024-02-12');

insert into VarieT values (default,'The vert',default,1.8);
insert into VarieT values (default,'The noir',default,1.8);
insert into VarieT values (default,'Tisane',default,1.8);

insert into parcelle values (default,120,1,'2023-10-12');
insert into parcelle values (default,20,2,'2024-01-10');
insert into parcelle values (default,200,3,'2024-02-01');
insert into parcelle values (default,80,4,'2023-12-30');


insert into Cueillette (id,idParcelle,quantite,idCueilleur,Dates) values (default,1,120,2,'2023-11-23');
insert into Cueillette (id,idParcelle,quantite,idCueilleur,Dates) values (default,2,140,3,'2024-05-11');
